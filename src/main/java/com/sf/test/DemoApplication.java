package com.sf.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.sf.test.configs.AppConfig;
import com.sf.test.models.Amount;
import com.sf.test.services.TransferService;

//@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		//SpringApplication.run(DemoApplication.class, args);
		//Load Spring context
				ConfigurableApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
				//Get TransferService bean
				TransferService transferService = applicationContext.getBean(TransferService.class);
				//Use transfer method
				transferService.transferAmmount(100l, 200l, new Amount(2000.0));
				applicationContext.close();
	}

}
