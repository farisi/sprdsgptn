package com.sf.test.repositories.jdbc;

import com.sf.test.models.Account;
import com.sf.test.models.Amount;
import com.sf.test.repositories.TransferRepository;

public class JdbcTransferRepository implements TransferRepository {

	@Override
	public void transfer(Account accountA, Account accountB, Amount amount) {
		// TODO Auto-generated method stub
		System.out.println("Transfering amount from account A to B via JDBC implementation");
	}

}
