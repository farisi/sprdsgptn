package com.sf.test.repositories.jdbc;

import com.sf.test.models.Account;
import com.sf.test.models.Amount;
import com.sf.test.repositories.AccountRepository;

public class JdbcAccountRepository implements AccountRepository {

	@Override
	public Account findByAccountId(Long accountId) {
		// TODO Auto-generated method stub
		return new Account(accountId, "Arnav Rajput", new Amount(3000.0));
	}

}
