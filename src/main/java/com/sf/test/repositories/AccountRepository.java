package com.sf.test.repositories;

import com.sf.test.models.Account;

public interface AccountRepository {
	Account findByAccountId(Long accountId);
}
