package com.sf.test.repositories;

import com.sf.test.models.Account;
import com.sf.test.models.Amount;

public interface TransferRepository {
	void transfer(Account accountA, Account accountB, Amount amount);
}
