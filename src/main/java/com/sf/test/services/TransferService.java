package com.sf.test.services;

import com.sf.test.models.Amount;

public interface TransferService {
	void transferAmmount(Long a, Long b, Amount amount);
}
