package com.sf.test.services;

import com.sf.test.models.Account;
import com.sf.test.models.Amount;
import com.sf.test.repositories.AccountRepository;
import com.sf.test.repositories.TransferRepository;

public class TransferServiceImpl implements TransferService {
	
	AccountRepository accountRepository;
	TransferRepository transferRepository;
	
	public TransferServiceImpl(AccountRepository accountRepository, TransferRepository transferRepository) {
		super();
		this.accountRepository = accountRepository;
		this.transferRepository = transferRepository;
	}

	@Override
	public void transferAmmount(Long a, Long b, Amount amount) {
		Account accountA = accountRepository.findByAccountId(a);
		Account accountB = accountRepository.findByAccountId(b);
		transferRepository.transfer(accountA, accountB, amount);
	}
}
