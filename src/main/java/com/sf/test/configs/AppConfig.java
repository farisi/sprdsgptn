package com.sf.test.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.sf.test.aspects.LoggingAspect;
import com.sf.test.repositories.AccountRepository;
import com.sf.test.repositories.TransferRepository;
import com.sf.test.repositories.jdbc.JdbcAccountRepository;
import com.sf.test.repositories.jdbc.JdbcTransferRepository;
import com.sf.test.services.TransferService;
import com.sf.test.services.TransferServiceImpl;

@Configuration
@EnableAspectJAutoProxy
public class AppConfig {
	
	@Bean
	public TransferService transferService(){
		return new TransferServiceImpl(accountRepository(), transferRepository());
	}
	@Bean
	public AccountRepository accountRepository() {
		return new JdbcAccountRepository();
	}
	@Bean
	public TransferRepository transferRepository() {
		return new JdbcTransferRepository();
	}
	
	@Bean
	public LoggingAspect loggingAspect() {
		return new LoggingAspect();
	}
}
